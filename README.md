# bs62

[![Crates.io](https://img.shields.io/crates/v/bs62?style=flat-square)](https://crates.io/crates/bs62)
[![License](https://img.shields.io/crates/l/bs62/0.1.3?style=flat-square)](https://gitlab.com/x3ro/bs62-rs/-/blob/main/LICENSE.txt)

A [Base62](https://en.wikipedia.org/wiki/Base62) encoder / decoder with
support for leading zero bytes.

Normally, during the conversion of base10 (decimal) to base62, the input data
is interpreted as one large number:

`[0x00, 0x13, 0x37] => 0x001337 => 4919 (decimal)`

As leading zeroes do not count to the value of a number (`001337 = 1337`),
they are ignored while converting the number to base62.

- This is exactly what the [`encode_num`] function does.
- The [`encode_data`] keeps these leading zeroes.

This is achieved by prepending a `0x01` byte to the data before encoding,
thus creating a number that starts with a `1`: `001337 => 1001337` (No zeroes
are removed)

The leading `0x01` is removed after the data has been decoded from bse62 back
to base10: `1001337 => 001337`



## Alphabet

```txt
0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz
```

This is the same alphabet that
[CyberChef](https://gchq.github.io/CyberChef/#recipe=To_Base62('0-9A-Za-z'))
uses by default: `[0-9][A-Z][a-z]`.

[Wikipedia/Base62](https://en.wikipedia.org/wiki/Base62) suggests another
alphabet (`[A-Z][a-z][0-9]`) but I found that many online converters use
either `[0-9][A-Z][a-z]` or `[0-9][a-z][A-Z]`. And as I love
[CyberChef](https://gchq.github.io/CyberChef/), I decided to use their
default alphabet aswell. I also think that starting with numbers is more
natural as base62 is actually a number system like decimal (which is actually
base10) or hexa-decimal (base16).


## Examples

### Convert Data to Base62

This method will prepend `0x01` to the data before encoding it.

```rust
let data = vec![0x13, 0x37];
let encoded = bs62::encode_data(&data);

assert_eq!(encoded, "IKN")
```



### Parse Base62 to Data

This method expects a leading `0x01` in the byte array after decoding. It
removes the first byte before returning the byte array.

```rust
let encoded = "IKN";
let data = bs62::decode_data(&encoded)?;

assert_eq!(data, vec![0x13_u8, 0x37]);
```


### Convert a Number to Base62

```rust
let num = 1337;
let encoded = bs62::encode_num(&num);

assert_eq!(encoded, "LZ")
```



### Parse Base62 to Number

```rust
let num = 1337;
let encoded = bs62::encode_num(&num);

assert_eq!(encoded, "LZ")
```

## License

`bs62` is licensed under MIT.

See the [LICENSE.txt](LICENSE.txt) file in this repository for more information.


[`encode_num`]: https://docs.rs/bs62/0.1.3/bs62/fn.encode_data.html
[`encode_data`]: https://docs.rs/bs62/0.1.3/bs62/fn.encode_num.html
