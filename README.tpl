# {{crate}}

[![Crates.io](https://img.shields.io/crates/v/bs62?style=flat-square)](https://crates.io/crates/bs62)
[![License](https://img.shields.io/crates/l/bs62/{{version}}?style=flat-square)](https://gitlab.com/x3ro/bs62-rs/-/blob/main/LICENSE.txt)

{{readme}}

## License

`{{crate}}` is licensed under {{license}}.

See the [LICENSE.txt](LICENSE.txt) file in this repository for more information.


[`encode_num`]: https://docs.rs/bs62/{{version}}/bs62/fn.encode_data.html
[`encode_data`]: https://docs.rs/bs62/{{version}}/bs62/fn.encode_num.html