# Checklist for Publishing to [crates.io](https://crates.io/)

## Preparations

```
$ cargo install cargo-bump
$ cargo install cargo-readme
```



## Checklist

- [ ] `cd` to project root

- [ ] Bump version in `Cargo.toml`

  ```
  cargo bump patch
  # or
  cargo bump minor
  # or
  cargo bump major
  ```

- [ ] Get crate version

  ```
  PKG_VERSION="$(awk -F '=' '/version/ { a = gensub(/.*?"(.*?)".*?/, "\\1", "g", $2); print a }' Cargo.toml)" ; echo -e "\n Version: $PKG_VERSION\n"
  ```

- [ ] Run tests
  
  ```
  cargo test --all
  ```

- [ ] Check documentation

  ```
  cargo doc --open
  ```

- [ ] Generate `README.md`

  ```
  cargo readme --no-indent-headings -o README.md
  ```

- [ ] Check generated `README.md`
  - [ ] Check links
  - [ ] Check examples don't include lines starting with `#`

- [ ] Commit updated `README.md`

  ```
  git add README.md
  git commit -m '📖 DOC: Generate `README.md` from cargo doc'
  ```

- [ ] Dry-run publish (check that only `Cargo.toml` has changes)

  ```
  cargo publish --dry-run
  ```

- [ ] Dry-run publish (check for warnings / errors)

  ```
  cargo publish --dry-run --allow-dirty
  ```

- [ ] Commit version change

  ```
  git add Cargo.toml
  git commit -m "🚀 RELEASE: Bump version to v${PKG_VERSION}"
  ```

- [ ] Add version tag
  
  ```
  git tag -m "" -a v${PKG_VERSION}
  ```

- [ ] Push branch and tags
  
  ```
  git push
  git push --tags
  ```

- [ ] Publish crate

  ```
  cargo publish
  ```
